package com.example.rohlik.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "shoporder")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@NotNull
	@PastOrPresent
	@Column(name = "created")
	private Date created;

	@Type(type = "true_false")
	@Column(name = "paid")
	private Boolean paid = false;

	@PastOrPresent
	@Column(name = "cancelled")
	private Date cancelled;

	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<OrderItem> orderItems = new ArrayList<>();

	public Order() {
		super();
		this.created = new Date();
	}

	public Order(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
		this.created = new Date();
	}

	public int getId() {
		return id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Boolean isPaid() {
		return paid == null ? false : paid;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	public Date getCancelled() {
		return cancelled;
	}

	public void setCancelled(Date cancelled) {
		this.cancelled = cancelled;
	}

	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}

}
