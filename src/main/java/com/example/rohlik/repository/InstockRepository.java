package com.example.rohlik.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.rohlik.entity.Instock;

@Repository
public interface InstockRepository extends JpaRepository<Instock, Object> {

	List<Instock> findByProductIdAndOrderItemIdIsNull(int productId);

	List<Instock> findByOrderItemId(Integer id);

}