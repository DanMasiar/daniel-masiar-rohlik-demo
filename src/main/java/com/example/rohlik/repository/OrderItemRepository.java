package com.example.rohlik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.rohlik.entity.OrderItem;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Object> {

}