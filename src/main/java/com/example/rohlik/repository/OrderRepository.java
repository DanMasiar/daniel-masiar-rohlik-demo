package com.example.rohlik.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.rohlik.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Object> {

}