package com.example.rohlik.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.rohlik.additional.MissingStock;
import com.example.rohlik.entity.Instock;
import com.example.rohlik.entity.Order;
import com.example.rohlik.entity.OrderItem;
import com.example.rohlik.entity.Product;
import com.example.rohlik.repository.InstockRepository;
import com.example.rohlik.repository.OrderRepository;

/**
 * This class manages order allocation.
 * 
 * @author Daniel Mäsiar
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OrderAllocator {

	private final InstockRepository instockRepo;
	private final OrderRepository orderRepo;

	@Autowired
	public OrderAllocator(InstockRepository instockRepo, OrderRepository orderRepo) {
		super();
		this.instockRepo = instockRepo;
		this.orderRepo = orderRepo;
	}

	/**
	 * Tries to allocate instocks needed for given order.
	 * 
	 * @param order Order to allocate
	 * @return Returns list of products and it's amount that is missing. Returns
	 *         empty list if order was allocated successfully (all items were on
	 *         stock).
	 */
	public List<MissingStock> allocateOrder(Order order) {
		final List<MissingStock> missing = new ArrayList<MissingStock>();

		for (final OrderItem orderItem : order.getOrderItems()) {

			final Product product = orderItem.getProduct();

			final List<Instock> freeInstocksForProduct = instockRepo
					.findByProductIdAndOrderItemIdIsNull(product.getId());

			final int neededAmount = orderItem.getQty();
			final int availableAmount = freeInstocksForProduct.size();

			if (neededAmount > availableAmount) {
				/* Add missing item into response */
				missing.add(new MissingStock(product.getId(), neededAmount - availableAmount));

				/*
				 * Un-allocate any allocated instocks.
				 */
				unallocateInstocksForOrderItems(order);

				/*
				 * Now we just finish the loop to see what else is missing.
				 */
				continue;
			}

			/*
			 * If at least one item is already missing, do not continue with allocation.
			 */
			if (!missing.isEmpty()) {
				continue;
			}

			/* Allocate needed amount */
			freeInstocksForProduct.stream().limit(neededAmount).forEach(instock -> instock.setOrderItem(orderItem));
		}

		return missing;
	}

	/**
	 * Unallocates any instocks associated with given order.
	 * 
	 * @param order Order for which should the instocks be unallocated. Must be
	 *              up-to-date.
	 */
	public void unallocateInstocksForOrderItems(Order order) {
		for (final OrderItem orderItem : order.getOrderItems()) {
			final List<Instock> instocks = instockRepo.findByOrderItemId(orderItem.getId());
			instocks.stream().forEach(instock -> {
				instock.setOrderItem(null);
				instockRepo.save(instock);
			});
		}
	}

	/**
	 * Checks if given order is paid. If not, it gets cancelled.
	 * 
	 * @param orderId ID of order to check.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cancelOrderIfNotPaid(int orderId) {
		final Order refreshedOrder = orderRepo.getOne(orderId);

		/* Cancel if order is not paid */
		if (refreshedOrder.isPaid() == null || !refreshedOrder.isPaid()) {

			/* And check if is not already cancelled (i.e. manual order cancellation) */
			if (refreshedOrder.getCancelled() == null)
				cancelOrder(refreshedOrder);
		}
	}

	/**
	 * Cancels given order by unallocating related instocks and marking it as
	 * cancelled.
	 * 
	 * @param order Order to cancel
	 */
	public void cancelOrder(Order order) {
		/* Unallocate any instocks locked by this order */
		unallocateInstocksForOrderItems(order);

		/* Mark order as cancelled */
		markOrderAsCancelled(order);
	}

	/**
	 * Marks given order as cancelled by filling the cancelled attribute with
	 * current datetime.
	 * 
	 * @param order Order to be cancelled.
	 */
	public void markOrderAsCancelled(Order order) {
		order.setCancelled(new Date());
		orderRepo.save(order);
	}

}
