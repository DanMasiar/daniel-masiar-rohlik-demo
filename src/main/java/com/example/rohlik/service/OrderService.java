package com.example.rohlik.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.rohlik.additional.MissingStock;
import com.example.rohlik.additional.OrderItemList;
import com.example.rohlik.entity.Order;
import com.example.rohlik.entity.OrderItem;
import com.example.rohlik.repository.OrderRepository;

@Service
@Transactional(rollbackFor = Exception.class)
public class OrderService {

	private final OrderRepository orderRepo;
	private final OrderItemService orderItemService;
	private final OrderAllocator orderAllocator;

	@Autowired
	public OrderService(OrderRepository orderRepo, OrderItemService orderItemService, OrderAllocator orderAllocator) {
		super();
		this.orderRepo = orderRepo;
		this.orderItemService = orderItemService;
		this.orderAllocator = orderAllocator;
	}

	/**
	 * Reads order with given ID from database;
	 * 
	 * @param orderId ID of the order.
	 * @return Order entity.
	 */
	public Order readOrder(int orderId) {
		return orderRepo.getOne(orderId);
	}

	/**
	 * Creates new order and populates it with given order item list.
	 * 
	 * @param orderItemList HashMap of pairs productId:amountToOrder.
	 * @return List of missing stock items. Empty list if order was places
	 *         successfully.
	 * @throws Exception Throws an exception if orderItemList is invalid.
	 */
	public List<MissingStock> createOrder(OrderItemList orderItemList) throws Exception {

		/* Validate */
		orderItemList.validate();

		/* Create new empty order */
		final Order order = orderRepo.save(new Order());

		/* Create order items for given order */
		final List<OrderItem> orderItems = orderItemService.createOrderItemsFromList(orderItemList, order);

		/* Add newly created order items to order */
		order.setOrderItems(orderItems);

		/* Try to allocate all order items and return any out-of-stock */
		final List<MissingStock> missingStockItems = orderAllocator.allocateOrder(order);

		/*
		 * If some items are missing, delete the order with the order items and return
		 * missing goods as response.
		 */
		if (!missingStockItems.isEmpty()) {
			orderRepo.delete(order);
		} else {
			/*
			 * Set timer to unallocate the order in case it is not paid for after 30 minutes
			 */
			CompletableFuture.delayedExecutor(30, TimeUnit.MINUTES).execute(() -> {
				orderAllocator.cancelOrderIfNotPaid(order.getId());
			});
		}

		return missingStockItems;
	}

	/**
	 * Cancels an order.
	 * 
	 * @param orderId ID of order to cancel
	 * @throws Exception
	 */
	public void cancelOrder(int orderId) throws Exception {
		final Order order = orderRepo.getOne(orderId);
		
		if (order == null)
			throw new Exception(String.format("Order with id %d not found.", orderId));
		
		orderAllocator.cancelOrder(order);
	}

	/**
	 * Marks order as paid.
	 * 
	 * @param orderId ID of order to mark as paid
	 * @throws Exception
	 */
	public void payOrder(int orderId) throws Exception {
		final Order order = orderRepo.getOne(orderId);
		
		if (order == null)
			throw new Exception(String.format("Order with id %d not found.", orderId));
		
		if (order.getCancelled() != null)
			throw new Exception("Order is already cancelled.");

		order.setPaid(true);
		orderRepo.save(order);
	}

}
