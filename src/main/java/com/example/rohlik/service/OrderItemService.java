package com.example.rohlik.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.rohlik.additional.OrderItemList;
import com.example.rohlik.entity.Order;
import com.example.rohlik.entity.OrderItem;
import com.example.rohlik.entity.Product;
import com.example.rohlik.repository.OrderItemRepository;
import com.example.rohlik.repository.ProductRepository;

@Service
@Transactional(rollbackFor = Exception.class)
public class OrderItemService {

	private final OrderItemRepository orderItemRepo;
	private final ProductRepository productRepo;

	@Autowired
	public OrderItemService(OrderItemRepository orderItemRepo, ProductRepository productRepo) {
		super();
		this.orderItemRepo = orderItemRepo;
		this.productRepo = productRepo;
	}

	/**
	 * Creates an OrderItem and saves it into DB.
	 * 
	 * @param product Product to put in the order item.
	 * @param qty     Amount of products in one order item.
	 * @param order   Order the order item belongs to.
	 * 
	 * @return Newly created OrderItem
	 */
	public OrderItem createOrderItem(Product product, Integer qty, Order order) {
		return orderItemRepo.save(new OrderItem(product, qty, order));
	}

	/**
	 * Creates orderItems from given OrderItemList and Order.
	 * 
	 * @param orderItemList
	 * @param order
	 * 
	 * @return List of newly created OrderItems
	 * @throws Exception Throws an exception when trying to create an order with
	 *                   deleted product
	 */
	public List<OrderItem> createOrderItemsFromList(OrderItemList orderItemList, Order order) throws Exception {
		final List<OrderItem> createdItems = new ArrayList<OrderItem>();

		try {
			orderItemList.getProductIDs().stream().forEach(productId -> {
				final Integer qty = orderItemList.get(productId);
				final Product product = productRepo.getOne(productId);

				if (product.isDeleted())
					throw new IllegalArgumentException(
							String.format("Product %s (id:%d) is deleted and cannot be placed in order.",
									product.getName(), product.getId()));

				createdItems.add(createOrderItem(product, qty, order));
			});
		} catch (IllegalArgumentException e) {
			throw new Exception(e.getLocalizedMessage());
		}
		return createdItems;
	}

}
