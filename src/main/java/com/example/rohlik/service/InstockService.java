package com.example.rohlik.service;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.rohlik.entity.Instock;
import com.example.rohlik.entity.Product;
import com.example.rohlik.repository.InstockRepository;

@Service
@Transactional(rollbackFor = Exception.class)
public class InstockService {

	private final ProductService productService;
	private final InstockRepository instockRepo;

	@Autowired
	public InstockService(ProductService productService, InstockRepository instockRepo) {
		super();
		this.productService = productService;
		this.instockRepo = instockRepo;
	}

	/**
	 * Creates an instock.
	 * 
	 * @param data ProductId and quantity of items to stock.
	 * @throws Exception When data is invalid, throws an exception.
	 */
	public void createInstock(Integer productId, Integer qty) throws Exception {

		/* Validate data. If invalid, throw an exception. */
		validateData(productId, qty);

		/* Get relevant product */
		final Product product = productService.readProduct(productId);
		if (product == null)
			throw new Exception(String.format("Product with id %d does not exist.", productId));

		/* Save instocks to DB */
		for (int i = 0; i < qty; i++)
			instockRepo.save(new Instock(product));

	}

	/**
	 * Validates data to create new instock.
	 * 
	 * @param productId ID of product.
	 * @param qty       Quantity of items to add to stock.
	 * @throws Exception An exception is thrown if validation fails.
	 * 
	 */
	private void validateData(Integer productId, Integer qty) throws Exception {
		if (productId == null)
			throw new Exception("Missing product id.");

		if (qty == null)
			throw new Exception("Missing quantity.");

		if (qty < 1)
			throw new Exception("Quantity must be 1 or higher.");

	}

}
