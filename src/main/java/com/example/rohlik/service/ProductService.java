package com.example.rohlik.service;

import java.math.BigDecimal;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.rohlik.entity.Product;
import com.example.rohlik.repository.ProductRepository;

@Service
@Transactional(rollbackFor = Exception.class)
public class ProductService {

	private final ProductRepository productRepo;

	@Autowired
	public ProductService(ProductRepository productRepo) {
		super();
		this.productRepo = productRepo;
	}

	/**
	 * Gets product with given productId from database.
	 * 
	 * @param productId ID of product to read.
	 * @return Product entity.
	 */
	public Product readProduct(int productId) {
		return productRepo.getOne(productId);
	}

	/**
	 * Adds product to database.
	 * 
	 * @param data Data about product - name and unit price.
	 * @throws Exception When data validation fails, an exception with an
	 *                   explanation message is thrown.
	 */
	public Product createProduct(String name, BigDecimal unitPrice) throws Exception {
		validateData(name, unitPrice);
		return productRepo.save(new Product(name, unitPrice));
	}

	/**
	 * Updates product with given info.
	 * 
	 * @param id        ID of product to update. ID must not be null.
	 * @param name      New name of the product. If null, the name won't be changed.
	 * @param unitPrice New unitPrice of the product. If null, the price won't be
	 *                  changed.
	 * @param deleted   Deleted status of the product. If null, the status won't be
	 *                  changed.
	 * @throws Exception When data validation fails, an exception with an
	 *                   explanation message is thrown. Also throws an exception
	 *                   when id is null.
	 */
	public Product updateProduct(Integer id, String name, BigDecimal unitPrice, Boolean deleted) throws Exception {
		if (id == null) {
			throw new Exception("ID must not be null.");
		}

		Product product = productRepo.getOne(id);

		if (deleted != null) {
			product.setDeleted(deleted);
		}

		if (name != null) {
			validateName(name);
			product.setName(name);
		}

		if (unitPrice != null) {
			validateUnitPrice(unitPrice);
			product.setUnitPrice(unitPrice);
		}

		return productRepo.save(product);

	}

	/**
	 * Validates data to create new product.
	 * 
	 * @param data Map containing product name and unit price.
	 * @throws Exception An exception is thrown in following cases: name is missing
	 *                   or is empty or is just whitespace or unitPrice is negative.
	 * 
	 */
	private void validateData(String name, BigDecimal unitPrice) throws Exception {

		validateName(name);
		validateUnitPrice(unitPrice);

	}

	private void validateName(String name) throws Exception {
		if (name == null || name.trim().isEmpty())
			throw new Exception("Name cannot be empty.");
	}

	private void validateUnitPrice(BigDecimal unitPrice) throws Exception {
		if (unitPrice.compareTo(BigDecimal.ZERO) < 0) {
			throw new Exception("Unit price must not be less than 0.");
		}
	}

}
