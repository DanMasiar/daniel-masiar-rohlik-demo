package com.example.rohlik.additional;

import java.util.HashMap;
import java.util.Set;

/**
 * OrderItemList is a helper HashMap which holds ids of ordered items and
 * ordered amounts
 * 
 * @author Daniel Mäsiar
 *
 */
public class OrderItemList extends HashMap<Integer, Integer> {

	private static final long serialVersionUID = 4100831543311920176L;

	public Set<Integer> getProductIDs() {
		return this.keySet();
	}

	/**
	 * Validates data.
	 * 
	 * @return False if some item is ordered less than once. True otherwise.
	 */
	public void validate() {

		this.getProductIDs().stream().forEach(productId -> {
			final Integer amount = get(productId);
			if (amount < 1)
				throw new RuntimeException(String.format("Product with id %d ordered %d times. Amount must be 1 or higher.", productId, amount));
		});
	}

}