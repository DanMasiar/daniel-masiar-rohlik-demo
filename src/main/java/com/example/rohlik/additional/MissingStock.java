package com.example.rohlik.additional;

/**
 * Item that holds missing product and an amount which is missing
 * 
 * @author Daniel Mäsiar
 *
 */
public class MissingStock {

	/* Product that is missing */
	private int productId;

	/* Amount that is missing */
	private int qty;

	public MissingStock(int productId, int qty) {
		super();
		this.productId = productId;
		this.qty = qty;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

}
