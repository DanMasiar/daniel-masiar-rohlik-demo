package com.example.rohlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.rohlik.service.InstockService;

/**
 * Instock controller
 * 
 * @author Daniel Mäsiar
 *
 */
@RestController
public class InstockController {

	private final InstockService instockService;

	private final static String INSTOCK = "/instock";

	@Autowired
	public InstockController(InstockService InstockService) {
		this.instockService = InstockService;
	}

	/**
	 * Creates a new instock.
	 * 
	 * @param productId ID of product to add to stock
	 * @param qty       Quantity of given product to add to stock.
	 * @throws Exception Throws an exception if data is invalid (null product id,
	 *                   non-positive amount...).
	 */
	@PostMapping(value = INSTOCK)
	public void createInstock(@RequestParam Integer productId, @RequestParam Integer qty) throws Exception {
		instockService.createInstock(productId, qty);
	}

	/**
	 * Handles any exception thrown by controller methods.
	 * 
	 * @param ex Exception
	 * @return ResponseEntity with status 400 and an error message.
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> handleException(Exception ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getLocalizedMessage());
	}

}
