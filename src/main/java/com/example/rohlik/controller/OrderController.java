package com.example.rohlik.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.rohlik.additional.MissingStock;
import com.example.rohlik.additional.OrderItemList;
import com.example.rohlik.entity.Order;
import com.example.rohlik.service.OrderService;

/**
 * Order controller
 * 
 * @author Daniel Mäsiar
 *
 */
@RestController
public class OrderController {

	private final OrderService orderService;

	private final static String ORDER = "/order";

	@Autowired
	public OrderController(OrderService orderService) {
		this.orderService = orderService;
	}

	/**
	 * Gets an order by its id.
	 * 
	 * @param orderId Id of order to get.
	 * @return Order entity.
	 */
	@GetMapping(value = ORDER + "/{orderId}")
	public Order readOrder(@PathVariable int orderId) {
		return orderService.readOrder(orderId);
	}

	/**
	 * Creates new order from given list of OrderItems.
	 * 
	 * @param data List of OrderItems to put in the order. Order is created only if
	 *             there is enough stock.
	 * @return List of MissingStock in case there are some items missing on stock.
	 * @throws Exception If any other error happens during creation an exception
	 *                   with error message is thrown.
	 */
	@PostMapping(value = ORDER)
	public List<MissingStock> createOrder(@RequestBody OrderItemList data) throws Exception {
		return orderService.createOrder(data);
	}

	/**
	 * Cancels the given order. During the cancellation, any items allocated on
	 * stock are made available again.
	 * 
	 * @param orderId Id of an order to cancel.
	 * @throws Exception 
	 */
	@PutMapping(value = ORDER + "/{orderId}/cancel")
	public void cancelOrder(@PathVariable int orderId) throws Exception {
		orderService.cancelOrder(orderId);
	}

	/**
	 * Marks order with given ID as paid.
	 * 
	 * @param orderId ID of order to pay.
	 * @throws Exception
	 */
	@PutMapping(value = ORDER + "/{orderId}/pay")
	public void payOrder(@PathVariable int orderId) throws Exception {
		orderService.payOrder(orderId);
	}

	/**
	 * Handles any exception thrown by controller methods.
	 * 
	 * @param ex Exception
	 * @return ResponseEntity with status 400 and an error message.
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> handleException(Exception ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getLocalizedMessage());
	}

}
