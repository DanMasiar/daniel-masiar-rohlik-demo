package com.example.rohlik.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.rohlik.service.ProductService;

/**
 * Product controller
 * 
 * @author Daniel Mäsiar
 *
 */
@RestController
public class ProductController {

	private static final String PRODUCT = "/product";

	private final ProductService productService;

	@Autowired
	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	/**
	 * Creates a new product with given data. Does not check for duplicate products.
	 * 
	 * @param name      Name of the product.
	 * @param unitPrice Unit price of the product.
	 * @throws Exception Throws an exception with error message in case the product
	 *                   creation fail.
	 */
	@PostMapping(value = PRODUCT)
	public void createProduct(@RequestParam String name, @RequestParam BigDecimal unitPrice) throws Exception {
		productService.createProduct(name, unitPrice);
	}

	/**
	 * Updates product with given data.
	 * 
	 * @param id        ID of product to update. ID must not be null.
	 * @param name      New name of the product. If null, the name won't be changed.
	 * @param unitPrice New unitPrice of the product. If null, the price won't be
	 *                  changed.
	 * @param deleted   Deleted status of the product. If null, the status won't be
	 *                  changed.
	 * @throws Exception Throws an exception if update fails.
	 */
	@PutMapping(value = PRODUCT)
	public void updateProduct(
			@RequestParam Integer id, 
			@RequestParam(required = false) String name,
			@RequestParam(required = false) BigDecimal unitPrice, 
			@RequestParam(required = false) Boolean deleted)
			throws Exception {
		productService.updateProduct(id, name, unitPrice, deleted);
	}

	/**
	 * Handles any exception thrown by controller methods.
	 * 
	 * @param ex Exception
	 * @return ResponseEntity with status 400 and an error message.
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> handleException(Exception ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getLocalizedMessage());
	}

}
