package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.rohlik.additional.MissingStock;
import com.example.rohlik.entity.Instock;
import com.example.rohlik.entity.Order;
import com.example.rohlik.entity.OrderItem;
import com.example.rohlik.entity.Product;
import com.example.rohlik.repository.InstockRepository;
import com.example.rohlik.repository.OrderRepository;
import com.example.rohlik.service.OrderAllocator;

@ExtendWith(MockitoExtension.class)
class OrderAllocatorTests {

	@Mock
	InstockRepository instockRepo;

	@Mock
	OrderRepository orderRepo;

	@InjectMocks
	OrderAllocator orderAllocator;

	Order order;
	Product product;

	@BeforeEach
	public void init() {
		product = new Product("Rohlik", new BigDecimal(1.0));

		List<OrderItem> orderItems = new ArrayList<OrderItem>();
		order = new Order();
		orderItems.add(new OrderItem(product, 1, order));
		order.setOrderItems(orderItems);
	}

	@Test
	void allocateOrderTest_noMissingStock() {
		List<Instock> freeInstocks = getNInstocksForProduct(10, product);

		when(instockRepo.findByProductIdAndOrderItemIdIsNull(product.getId())).thenReturn(freeInstocks);

		List<MissingStock> missingStocks = orderAllocator.allocateOrder(order);

		assertTrue(missingStocks.isEmpty());

	}

	@Test
	void allocateOrderTest_someMissingStock() {
		List<Instock> freeInstocks = new ArrayList<Instock>();

		when(instockRepo.findByProductIdAndOrderItemIdIsNull(product.getId())).thenReturn(freeInstocks);

		List<MissingStock> missingStocks = orderAllocator.allocateOrder(order);

		assertTrue(missingStocks.size() == 1);

	}

	private List<Instock> getNInstocksForProduct(int n, Product product) {
		List<Instock> instocks = new ArrayList<Instock>();
		for (int i = 0; i < n; i++)
			instocks.add(new Instock(product));
		return instocks;
	}

}
