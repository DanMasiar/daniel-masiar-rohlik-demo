package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.rohlik.additional.OrderItemList;
import com.example.rohlik.entity.Order;
import com.example.rohlik.entity.OrderItem;
import com.example.rohlik.entity.Product;
import com.example.rohlik.repository.OrderItemRepository;
import com.example.rohlik.repository.ProductRepository;
import com.example.rohlik.service.OrderItemService;

@ExtendWith(MockitoExtension.class)
class OrderItemServiceTests {

	@Mock
	OrderItemRepository orderItemRepo;

	@Mock
	ProductRepository productRepo;

	@InjectMocks
	OrderItemService orderItemService;

	@BeforeEach
	void init() {

	}

	@Test
	void createOrderItemsFromList_orderItemExist() {

		OrderItemList orderItemList = new OrderItemList();
		orderItemList.put(1, 10);

		Order order = new Order();

		when(orderItemRepo.save(any(OrderItem.class))).thenReturn(new OrderItem(new Product(), 10, order));
		when(productRepo.getOne(any(int.class))).thenReturn(new Product());

		try {
			List<OrderItem> orderItems = orderItemService.createOrderItemsFromList(orderItemList, order);
			assertTrue(orderItems.size() == 1 && orderItems.get(0).getQty() == 10);
		} catch (Exception e) {
			fail();
		}

	}

}
