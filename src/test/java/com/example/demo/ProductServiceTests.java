package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.rohlik.entity.Product;
import com.example.rohlik.repository.ProductRepository;
import com.example.rohlik.service.ProductService;

@ExtendWith(MockitoExtension.class)
class ProductServiceTests {

	@Mock
	ProductRepository productRepo;

	@InjectMocks
	ProductService productService;

	Product testProduct;

	@BeforeEach
	void init() {
		testProduct = new Product("Rohlik", new BigDecimal(1.0));
	}

	@Test
	void createProduct_validationPassOk() {

		when(productRepo.save(any(Product.class))).thenReturn(testProduct);

		assertDoesNotThrow(() -> {
			productService.createProduct("Rohlik", new BigDecimal(1.0));
		});

	}

	@Test
	void createProduct_productHasNameAndUnitPrice() {

		when(productRepo.save(any(Product.class))).thenReturn(testProduct);

		try {
			Product created = productService.createProduct("Rohlik", new BigDecimal(1.0));
			assertTrue(created.getName() != null && !created.getName().trim().isEmpty()
					&& created.getUnitPrice() != null && created.getUnitPrice().compareTo(new BigDecimal(0)) > 0);
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	void dataValidationThrowsExceptionWhenIncorrect_nullName() {

		assertThrows(Exception.class, () -> {
			productService.createProduct(null, new BigDecimal(1.0));
		});

	}

	@Test
	void dataValidationThrowsExceptionWhenIncorrect_emptyName() {

		assertThrows(Exception.class, () -> {
			productService.createProduct(" ", new BigDecimal(1.0));
		});

	}

	@Test
	void dataValidationThrowsExceptionWhenIncorrect_unitPriceLessThan0() {

		assertThrows(Exception.class, () -> {
			productService.createProduct("Rohlik", new BigDecimal(-1.0));
		});

	}

	@Test
	void deleteProduct_isDeleted() {
		when(productRepo.save(any(Product.class))).then(returnsFirstArg());
		when(productRepo.getOne(any(int.class))).thenReturn(testProduct);

		try {
			Product deleted = productService.updateProduct(1, "Rohlik", new BigDecimal(1.0), true);
			assertTrue(deleted.isDeleted());
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	void deleteProduct_idIsRequired() {
		assertThrows(Exception.class, () -> {
			productService.updateProduct(null, "Rohlik", new BigDecimal(1.0), false);
		});

	}

}
